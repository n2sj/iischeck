echo off
rem ########################################
rem Initialize
rem ########################################
rem Set environmental variable
set workdir=c:\temp
set targetfile=%workdir%\*.log
set workfile=%workdir%\aaaa.tmp
set zerofile=%workdir%\zero.tmp
set targetstring=Error
set intervalsec=60

rem create zero.tmp
echo "" | find "@@" > %zerofile%


rem ########################################
rem Loop Initialize
rem ########################################

rem ########################################
rem Main Loop
rem ########################################

: loop_a
   echo off
   set target_file=*.log
  
   rem ワークファイルをあらかじめ削除
   del %workfile%

   rem ここでgrepしてワークファイルに出力する
   grep -i -r "%targetstring%" %targetfile% > %workfile%

   rem if exist(workfile) then sendmail
   fc /b %workfile% %zerofile% | find "FC: 相違点は検出されませんでした" > NUL
   if "%errorlevel%"=="0" goto skip
     echo "There is error Message"
     goto loopend 
     
   :skip
     echo "No error"

   :loopend

   timeout %intervalsec%
goto :loop_a
